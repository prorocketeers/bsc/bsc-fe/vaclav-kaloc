import React from "react";
import { NotFound } from "../../components/NotFound";

export const PageNotFound = () => {
  return <NotFound />;
};
