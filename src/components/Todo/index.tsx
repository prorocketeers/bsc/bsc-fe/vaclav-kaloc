import React, { useContext } from "react";
import List from "antd/lib/list";
import Checkbox from "antd/lib/checkbox";
import Typography from "antd/lib/typography";
import Col from "antd/lib/col";
import Icon from "antd/lib/icon";
import Row from "antd/lib/row";
import { ITodoProps } from "./interaces";
import { TodosContext } from "../../context/TodoContext";
import { useHistory } from "react-router-dom";

const { Text } = Typography;

export const Todo: React.FC<ITodoProps> = ({
  todo: { id, title, completed }
}) => {
  const todosContext = useContext(TodosContext);
  const history = useHistory();

  return (
    <List.Item>
      <Row type="flex" style={{ width: "100%" }}>
        <Col xs={2} lg={1}>
          <Checkbox
            key={id}
            defaultChecked={completed}
            onChange={() => todosContext.handleCheckboxToggle(id)}
          />
        </Col>
        <Col xs={18} lg={21} style={{ textAlign: "left" }}>
          <Text delete={completed}>{title}</Text>
        </Col>
        <Col xs={2} lg={1}>
          <Icon
            type="edit"
            theme="twoTone"
            twoToneColor="#031428"
            onClick={() => history.push(`/todo/${id}`)}
          />
        </Col>

        <Col xs={2} lg={1}>
          <Icon
            onClick={() => todosContext.handleTodoDelete(id)}
            type="delete"
            theme="twoTone"
            twoToneColor="#F41C23"
          />
        </Col>
      </Row>
    </List.Item>
  );
};
