import styled from "styled-components";
import Button from "antd/lib/button";

export const StyledButton = styled(Button)`
  margin-top: 4px;

  .ant-typography {
    color: white;
  }
`;
