import { WrappedFormUtils } from "antd/lib/form/Form";

export interface IAddTodoProps {
  form: WrappedFormUtils;
}
