import styled from "styled-components";
import List from "antd/lib/list";

export const StyledList = styled(List)`
  background-color: white;
`;
