import React, { useContext } from "react";
import { StyledList } from "./styled";
import { Todo } from "../Todo";
import { TodosContext } from "../../context/TodoContext";

export const TodosList: React.FC = () => {
  const todosContext = useContext(TodosContext);

  return (
    <StyledList
      loading={todosContext.loading}
      bordered
      dataSource={todosContext.items}
      renderItem={(item: any) => <Todo key={item.id} todo={item} />}
      style={{ margin: "20px 0" }}
    />
  );
};
