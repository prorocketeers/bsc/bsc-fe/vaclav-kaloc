import styled from "styled-components";
import { Layout } from "antd";

const { Content } = Layout;
export const StyledPageLayout = styled(Layout)`
  text-align: center;
  font-size: calc(10px + 2vmin);
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`;

export const StyledContent = styled(Content)`
  &.ant-layout-content {
    background-color: #282c34;
    min-height: 85vh;
    color: white;
  }
`;
