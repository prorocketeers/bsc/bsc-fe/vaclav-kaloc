import React from "react";
import { Col, Row } from "antd";
import { Header } from "../Header";
import { Footer } from "../Footer";
import { StyledPageLayout, StyledContent } from "./styled";
import "antd/dist/antd.css";

const PageLayout: React.FC = ({ children }) => {
  return (
    <StyledPageLayout>
      <Header />
      <StyledContent>
        <Row>
          <Col xs={2} sm={4} md={6} />
          <Col xs={20} sm={16} md={12}>
            {children}
          </Col>
          <Col xs={2} sm={4} md={6} />
        </Row>
      </StyledContent>
      <Footer />
    </StyledPageLayout>
  );
};

export default PageLayout;
