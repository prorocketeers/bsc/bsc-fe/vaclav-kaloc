import styled from "styled-components";
import { Layout } from "antd";
const { Footer } = Layout;

export const StyledFooter = styled(Footer)`
  width: 100%;
  min-height: 5vh;
`;
