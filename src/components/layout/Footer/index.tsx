import React from "react";
import Icon from "antd/lib/icon";
import { StyledFooter } from "./styled";

export const Footer: React.FC = () => {
  return (
    <StyledFooter>
      <Icon type="copyright" />
      Václav Kaloč
    </StyledFooter>
  );
};
