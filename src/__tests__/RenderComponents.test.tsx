import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Homepage } from "../pages/Homepage";
import AddTodo from "../components/AddTodo";
import { TodosList } from "../components/TodosList";

configure({ adapter: new Adapter() });

describe("RenderComponents tests", () => {
  const wrapper = shallow(<Homepage />);
  it("should have a AddTodo component", () => {
    expect(wrapper.find(AddTodo)).toHaveLength(1);
  });

  it("should have TodosList component", () => {
    expect(wrapper.find(TodosList)).toHaveLength(1);
  });
});
