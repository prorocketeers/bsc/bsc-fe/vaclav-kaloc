import React from "react";

export const CzechFlag = () => (
  <svg
    t="1575398897908"
    className="icon"
    viewBox="0 0 1024 1024"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    p-id="547"
    xlink="http://www.w3.org/1999/xlink"
    width="25"
    height="25"
  >
    <defs>
      <style type="text/css"></style>
    </defs>
    <path
      d="M512 512m-512 0a512 512 0 1 0 1024 0 512 512 0 1 0-1024 0Z"
      fill="#F0F0F0"
      p-id="548"
    ></path>
    <path
      d="M467.478 512S150.26 874.11 149.96 874.038C242.612 966.692 370.614 1024 512 1024c282.768 0 512-229.232 512-512H467.478z"
      fill="#D80027"
      p-id="549"
    ></path>
    <path
      d="M149.96 149.96c-199.948 199.948-199.948 524.13 0 724.08L512 512 149.96 149.96z"
      fill="#0052B4"
      p-id="550"
    ></path>
  </svg>
);
