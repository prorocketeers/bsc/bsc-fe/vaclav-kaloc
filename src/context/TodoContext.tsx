import React, { createContext, useState } from "react";
import axios from "axios";
import { message } from "antd";
import { useTranslation } from "react-i18next";
import { ITodo } from "../components/Todo/interaces";
import { TodoContext } from "./interaces";
import { findIndex, propEq, update } from "ramda";

export const TodosContext = createContext<TodoContext>({
  items: [],
  loading: false,
  fetchTodos: () => {}, // eslint-disable-line @typescript-eslint/no-empty-function
  fetchTodoDetail: async id => null,
  handleCheckboxToggle: async id => {}, // eslint-disable-line @typescript-eslint/no-empty-function
  handleTodoUpdate: async (id, text) => {}, // eslint-disable-line @typescript-eslint/no-empty-function
  handleTodoDelete: async id => {}, // eslint-disable-line @typescript-eslint/no-empty-function
  handleTodoCreate: async text => {} // eslint-disable-line @typescript-eslint/no-empty-function
});

export const TodosContextProvider: React.FC = ({ children }) => {
  const { t } = useTranslation();
  const [items, setItems] = useState<ITodo[]>([]);
  const [loading, setLoading] = useState(true);

  const fetchTodos = async () => {
    try {
      const result = await axios.get(`${process.env.REACT_APP_API_URL}`);
      setItems(result.data.slice(0, 10));
    } catch (err) {
      console.error(err);
      message.error(t("error"));
    }
    setLoading(false);
  };

  const handleCheckboxToggle = async (id: number) => {
    setLoading(true);
    try {
      const index = findIndex(propEq("id", id))(items);
      const updatedTodo = items[index];
      const completed = !updatedTodo.completed;
      const result = await axios.patch(
        `${process.env.REACT_APP_API_URL}/${id}`,
        {
          ...updatedTodo,
          completed
        }
      );
      setItems(update(index, result.data, items));
    } catch (err) {
      console.error(err);
      message.error(t("error"));
    }
    setLoading(false);
  };

  const handleTodoUpdate = async (id: number, title: string) => {
    setLoading(true);
    try {
      const index = findIndex(propEq("id", id))(items);
      const result = await axios.patch(
        `${process.env.REACT_APP_API_URL}/${id}`,
        {
          ...items[index],
          title
        }
      );
      setItems(update(index, result.data, items));
    } catch (err) {
      console.error(err);
      message.error(t("error"));
    }
    setLoading(false);
  };

  const handleTodoDelete = async (id: number) => {
    setLoading(true);
    try {
      const result = await axios.delete(
        `${process.env.REACT_APP_API_URL}/${id}`
      );
      if (result.status !== 200) {
        message.error(t("error"));
        return;
      }
      setItems(items.filter(todo => todo.id !== id));
      message.success(t("successDelete"));
    } catch (err) {
      console.error(err);
      message.error(t("error"));
    }
    setLoading(false);
  };

  const handleTodoCreate = async (text: string) => {
    setLoading(true);
    try {
      const result = await axios.post(`${process.env.REACT_APP_API_URL}`, {
        title: text,
        completed: false
      });
      if (result.status !== 201) {
        message.error(t("error"));
        return;
      }
      const todo = {
        ...result.data,
        // Creating unique id because mock api always returns the same id
        id: Math.floor(Math.random() * 180) + 20
      };

      setItems([...items, todo]);
      message.success(t("successAdd"));
    } catch (err) {
      console.error(err);
      message.error(t("error"));
    }
    setLoading(false);
  };

  const fetchTodoDetail = async (id: number): Promise<ITodo | null> => {
    setLoading(true);
    try {
      const result = await axios.get(`${process.env.REACT_APP_API_URL}/${id}`);
      setLoading(false);
      return result.data;
    } catch (err) {
      console.error(err);
      message.error(t("error"));
      setLoading(false);
      return null;
    }
  };

  const todosContext = {
    items,
    loading,
    fetchTodos,
    fetchTodoDetail,
    handleCheckboxToggle,
    handleTodoUpdate,
    handleTodoDelete,
    handleTodoCreate
  };

  return (
    <TodosContext.Provider value={todosContext}>
      {children}
    </TodosContext.Provider>
  );
};
