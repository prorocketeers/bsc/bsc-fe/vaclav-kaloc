module.exports = {
  parser: "@typescript-eslint/parser", // Specifies the ESLint parser
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },
  extends: [
    "eslint:recommended",
    "google",
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin,
    "plugin:eslint-comments/recommended",
    "plugin:react/recommended", // Uses the recommended rules from @eslint-plugin-react
    "prettier",
    "prettier/react"
  ],
  plugins: [
    "@typescript-eslint/eslint-plugin",
    "better-styled-components",
    "import",
    "jsx-a11y",
    "prettier",
    "react",
    "react-hooks"
  ],
  rules: {
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/explicit-member-accessibility": "off",
    "@typescript-eslint/interface-name-prefix": "off",
    "@typescript-eslint/prefer-interface": "off",
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "no-console": "off",
    "react/prop-types": "off",
    "@typescript-eslint/ban-ts-ignore": "off",
    "eslint-comments/no-unlimited-disable": "off"
  },
  env: {
    node: true,
    browser: true,
    es6: true
  },
  settings: {
    react: {
      version: "detect" // Tells eslint-plugin-react to automatically detect the version of React to use
    }
  }
};
